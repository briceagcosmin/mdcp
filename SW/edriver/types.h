/*********************************************************************************************************************
  @Author             : Bebe-Cosmin Briceag
  @Module Description : Generic types
  @Revision           : 1.0
  *******************************************************************************************************************

  @Naming Convention               : Below is presented the template used for naming convention
  @Public Function                 : <Module>_<TypeReturned>_<Description>
  @Private Function                : <Module>_<TypeReturned>_<Description>
  @Local Variables                 : <SizeOfType>_<Description>
  @Global Variables                : <Module>_<SizeOfType>_<Description>
  @Defines used as flags           : <Module>_<SizeOfType>_<DESCRIPTION>_<FLAG>          ((cast to)Decimal Number)
  @Defines used as masks           : <Module>_<SizeOfType>_<DESCRIPTION>_<MASK>          ((cast to)Hex Number)
  @Defines used as compiler switch : <Module>_<DESCRIPTION>                              (Decimal Number)


********************************************************************************************************************/

/*------------------------------ P R O T E C T I O N - D O U B L E - I N C L U S I O N ----------------------------*/

#ifndef _TYPES_H_
#define _TYPES_H_


typedef unsigned char      ubyte;
typedef signed char        sbyte;
typedef unsigned short     uword;
typedef signed   short     sword;
typedef unsigned long      ulong;
typedef signed long        slong;
typedef unsigned long long ullong;
typedef signed   long long sllong;
typedef void tvoid;
typedef unsigned char       tbool;
#define FALSE (tbool)0
#define TRUE  (tbool)1

#define null (void*)0




#endif
/*------------------ E N D - O F - P R O T E C T I O N - D O U B L E - I N C L U S I O N --------------------------*/
