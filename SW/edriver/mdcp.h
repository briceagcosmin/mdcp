/*********************************************************************************************************************
  @Author             :
  @Module Description :
  @Revision           :
 *******************************************************************************************************************

  @Naming Convention               : Below is presented the template used for naming convention
  @Public Function                 : <Module>_<TypeReturned>_<Description>
  @Private Function                : <Module>_<TypeReturned>_<Description>
  @Local Variables                 : <SizeOfType>_<Description>
  @Global Variables                : <Module>_<SizeOfType>_<Description>
  @Defines used as flags           : <Module>_<SizeOfType>_<DESCRIPTION>_<FLAG>          ((cast to)Decimal Number)
  @Defines used as masks           : <Module>_<SizeOfType>_<DESCRIPTION>_<MASK>          ((cast to)Hex Number)
  @Defines used as compiler switch : <Module>_<DESCRIPTION>                              (Decimal Number)


 ********************************************************************************************************************/

/*------------------------------ P R O T E C T I O N - D O U B L E - I N C L U S I O N ----------------------------*/

#ifndef _MDCP_H_
#define _MDCP_H_


/*--------------------------------------------- I N C L U D E S ---------------------------------------------------*/


/*-------------------------------------- E N D - O F - I N C L U D E S --------------------------------------------*/
typedef struct
{
	ubyte ub_FID;
	ubyte ub_SrcDest;
	uword uw_SizeOfData;
	ubyte *pub_Data;

}Frame;

typedef struct
{
	tvoid (*pfct_ErrorHandler)(tvoid);                /* In case an error frame is receive, it is up to the user what should be done*/
	tvoid (*pfct_Send)(const ubyte*,const uword);    /* Routine for sending data to the lower layer */
	tvoid (*pfct_Route)(const Frame *pcs_Frame);
	ubyte *pub_RxFrame;                               /* Location of the driver buffer */
	uword  uw_BusType;                                /* The type/types of the bus/buses used */
	tbool  b_DataReady;                               /* Announce the MDCP driver when data is ready (a complete frame is received) */
	ubyte  ub_OwnerAddress;                           /* Owner address */
}Bus;


/*----------------------------------- E X P O R T E D - F U N C T I O N S -----------------------------------------*/
extern tvoid MDCP_v_Run (tvoid);
extern tvoid MDCP_v_Init(tvoid);

/*!
      \sa MDCP_b_RequestRxRegion(const Bus* const cpcs_BusObject)
      \param cpcs_BusObject has direction in - It represents the newly created bus object to be registered
      \return TRUE : If the object is registered successfully
              FALSE: If the object was not registered successfully. Most probably,there is no more room left
 */
extern tbool MDCP_b_RequestBusRegion(const Bus* const cpcs_BusObject);





/*---------------------------- E N D - O F - E X P O R T E D - F U N C T I O N S ----------------------------------*/






#endif /* _MDCP_H_ */
/*------------------ E N D - O F - P R O T E C T I O N - D O U B L E - I N C L U S I O N --------------------------*/
