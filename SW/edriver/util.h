/*********************************************************************************************************************
  @Author             : Bebe-Cosmin Briceag
  @Module Description : Helpers functions used during the development
  @Revision           : 1.0
  *******************************************************************************************************************

  @Naming Convention               : Below is presented the template used for naming convention
  @Public Function                 : <Module>_<TypeReturned>_<Description>
  @Private Function                : <Module>_<TypeReturned>_<Description>
  @Local Variables                 : <SizeOfType>_<Description>
  @Global Variables                : <Module>_<SizeOfType>_<Description>
  @Defines used as flags           : <Module>_<SizeOfType>_<DESCRIPTION>_<FLAG>          ((cast to)Decimal Number)
  @Defines used as masks           : <Module>_<SizeOfType>_<DESCRIPTION>_<MASK>          ((cast to)Hex Number)
  @Defines used as compiler switch : <Module>_<DESCRIPTION>                              (Decimal Number)


********************************************************************************************************************/

/*------------------------------ P R O T E C T I O N - D O U B L E - I N C L U S I O N ----------------------------*/

#ifndef _UTIL_H_
#define _UTIL_H_


/*--------------------------------------------- I N C L U D E S ---------------------------------------------------*/


/*-------------------------------------- E N D - O F - I N C L U D E S --------------------------------------------*/




/*----------------------------------- E X P O R T E D - F U N C T I O N S -----------------------------------------*/
static inline tvoid UTIL_v_MemSet(tvoid* pv_RamDest,const ubyte cub_Fill,const ulong cul_Size)
{
	ulong  ul_Index = cul_Size;
	ubyte* pub_Dest = (ubyte*)pv_RamDest;

	while(ul_Index != (ulong)0)
	{
		*pub_Dest++ = cub_Fill;
		ul_Index--;
	}
}

static inline tvoid UTIL_v_MemCopy(tvoid* pv_Dest, const tvoid* pcv_Source, const ulong cul_Size)
{
	ubyte* pub_Dest   = (ubyte*)pv_Dest;
	ubyte* pub_Source = (ubyte*)pcv_Source;
	ulong  ul_Len     = cul_Size;

	while(ul_Len != (ulong)0)
	{
		*pub_Dest++ = *pub_Source;
		ul_Len--;
	}

}

/*---------------------------- E N D - O F - E X P O R T E D - F U N C T I O N S ----------------------------------*/






#endif
/*------------------ E N D - O F - P R O T E C T I O N - D O U B L E - I N C L U S I O N --------------------------*/
