/*********************************************************************************************************************
  @Author             : Bebe-Cosmin Briceag
  @Module Description : This module provides all the needed ids
  @Revision           : 1.0
  *******************************************************************************************************************

  @Naming Convention               : Below is presented the template used for naming convention
  @Public Function                 : <Module>_<TypeReturned>_<Description>
  @Private Function                : <Module>_<TypeReturned>_<Description>
  @Local Variables                 : <SizeOfType>_<Description>
  @Global Variables                : <Module>_<SizeOfType>_<Description>
  @Defines used as flags           : <Module>_<SizeOfType>_<DESCRIPTION>_<FLAG>          ((cast to)Decimal Number)
  @Defines used as masks           : <Module>_<SizeOfType>_<DESCRIPTION>_<MASK>          ((cast to)Hex Number)
  @Defines used as compiler switch : <Module>_<DESCRIPTION>                              (Decimal Number)


********************************************************************************************************************/

/*------------------------------ P R O T E C T I O N - D O U B L E - I N C L U S I O N ----------------------------*/

#ifndef _MDCP_FIDS_H_
#define _MDCP_FIDS_H_


/*--------------------------------------------- I N C L U D E S ---------------------------------------------------*/


/*-------------------------------------- E N D - O F - I N C L U D E S --------------------------------------------*/
#define MDCP_ub_ERROR_FID                    (ubyte)0x00                     /*!< Used to indicate an error frame  */
#define MDCP_ub_FWD_FID                      (ubyte)0x01                     /*!< Forwarding the frame to the destination */
#define MDCP_ub_ONE_SHOT_MEASUREMENT_FID     (ubyte)0x02                     /*!< Request to start a measurement with the address specified in the DATA field */
#define MDCP_ub_RESPONSE_FID                 (ubyte)0x03                     /*!< Identify the response FID  */
#define MCDP_ub_ROUTE_FID                    (ubyte)0x04                     /*!< Frame must be routed to another device */

#define MDCP_ub_MAX_NUMBER_OF_FIDS           (ubyte)5
/*----------------------------------- E X P O R T E D - F U N C T I O N S -----------------------------------------*/


/*---------------------------- E N D - O F - E X P O R T E D - F U N C T I O N S ----------------------------------*/






#endif /* _MDCP_FIDS_H_ */
/*------------------ E N D - O F - P R O T E C T I O N - D O U B L E - I N C L U S I O N --------------------------*/
