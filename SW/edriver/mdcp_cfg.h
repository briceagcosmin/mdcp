/*********************************************************************************************************************
  @Author             : Bebe-Cosmin Briceag
  @Module Description : Configuration of the MDCP.
                        IF YOU MAKE ANY MODIFICATION HERE, YOUR DATA WILL BE LOST. THIS FILE IS GENERATED
  @Revision           : 1.0
  *******************************************************************************************************************

  @Naming Convention               : Below is presented the template used for naming convention
  @Public Function                 : <Module>_<TypeReturned>_<Description>
  @Private Function                : <Module>_<TypeReturned>_<Description>
  @Local Variables                 : <SizeOfType>_<Description>
  @Global Variables                : <Module>_<SizeOfType>_<Description>
  @Defines used as flags           : <Module>_<SizeOfType>_<DESCRIPTION>_<FLAG>          ((cast to)Decimal Number)
  @Defines used as masks           : <Module>_<SizeOfType>_<DESCRIPTION>_<MASK>          ((cast to)Hex Number)
  @Defines used as compiler switch : <Module>_<DESCRIPTION>                              (Decimal Number)


********************************************************************************************************************/

/*------------------------------ P R O T E C T I O N - D O U B L E - I N C L U S I O N ----------------------------*/

#ifndef _MDCP_CFG_H_
#define _MDCP_CFG_H_


/*--------------------------------------------- I N C L U D E S ---------------------------------------------------*/


/*-------------------------------------- E N D - O F - I N C L U D E S --------------------------------------------*/
#define MDCP_OWNER_ADDRESS 0xB0
#define MDCP_DEBUG_MODE   1

#define MDCP_FLEXRAY_USED  0
#define MDCP_CAN_USED      1
#define MDCP_LIN_USED      0
#define MDCP_KLINE_USED    0
#define MDCP_USB_USED      0
#define MDCP_ETHERNET_USED 0
#define MDCP_SPI_USED      0
#define MDCP_I2C_USED      0
#define MDCP_CANFD_USED    0

#define MDCP_uw_MAX_BUSES_USED (uword)(MDCP_FLEXRAY_USED + \
								MDCP_CAN_USED + MDCP_LIN_USED + \
								MDCP_KLINE_USED + MDCP_USB_USED + \
								MDCP_ETHERNET_USED + MDCP_CANFD_USED)

/* FIXME Logic for bus type as an index */
#if (MDCP_FLEXRAY_USED == 1)
#define MDCP_uw_FLEXRAY_BUS   ((uword)1)
#endif

#if (MDCP_CAN_USED == 1)
#define MDCP_uw_CAN_BUS       ((uword)0)
#endif

#if (MDCP_LIN_USED == 1 )
#define MDCP_uw_LIN_BUS       ((uword)1)
#endif

#if (MDCP_KLINE_USED == 1)
#define MDCP_uw_KLINE_BUS     ((uword)0x03)
#endif

#if (MDCP_USB_USED == 1)
#define MDCP_uw_USB_BUS       ((uword)0x04)
#endif

#if (MDCP_ETHERNET_USED == 1)
#define MDCP_uw_ETHERNET_BUS  ((uword)0x05)
#endif

#if (MDCP_SPI_USED == 1)
#define MDCP_uw_SPI_BUS       ((uword)0x06)
#endif

#if (MDCP_I2C_USED == 1)
#define MDCP_uw_I2C_BUS       ((uword)0x07)
#endif

#if (MDCP_CANFD_USED == 1)
#define MDCP_uw_NAFD_BUS     ((uword)0x08)
#endif


/*----------------------------------- E X P O R T E D - F U N C T I O N S -----------------------------------------*/


/*---------------------------- E N D - O F - E X P O R T E D - F U N C T I O N S ----------------------------------*/






#endif /* _MDCP_CFG_H_ */
/*------------------ E N D - O F - P R O T E C T I O N - D O U B L E - I N C L U S I O N --------------------------*/
