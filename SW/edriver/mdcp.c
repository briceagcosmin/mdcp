/*********************************************************************************************************************
  @Author             : Bebe-Cosmin Briceag
  @Module Description : MDCP executes all the sub-layered protocols
  @Revision           : 1.0
 *******************************************************************************************************************

  @Naming Convention               : Below is presented the template used for naming convention
  @Public Function                 : <Module>_<TypeReturned>_<Description>
  @Private Function                : <Module>_<TypeReturned>_<Description>
  @Local Variables                 : <SizeOfType>_<Description>
  @Global Variables                : <Module>_<SizeOfType>_<Description>
  @Defines used as flags           : <Module>_<SizeOfType>_<DESCRIPTION>_<FLAG>          ((cast to)Decimal Number)
  @Defines used as masks           : <Module>_<SizeOfType>_<DESCRIPTION>_<MASK>          ((cast to)Hex Number)
  @Defines used as compiler switch : <Module>_<DESCRIPTION>                              (Decimal Number)


 ********************************************************************************************************************/


/*----------------------------------------------- I N C L U D E S -------------------------------------------------*/
#include "types.h"
#include "mdcp_cfg.h"
#include "mdcp_fids.h"
#include "crypto.h"
#include "util.h"
#include "mdcp.h"

#if (MDCP_DEBUG_MODE == 1)
#include <stdio.h>
#include <stdlib.h>
#endif
/*----------------------------------------- E N D - O F - I N C L U D E S -----------------------------------------*/



/*------------------------------------------- M O D U L E - S C O P E ---------------------------------------------*/



/*------------------------------------ E N D - O F - M O D U L E - S C O P E --------------------------------------*/
typedef struct
{
	tvoid (*pfct_FIDHandler)(const Bus* pcs_BusObject);
	ubyte ub_FID;
	ubyte ub_Reserved[3];
}FidLink;




/*-------------------------------------------- F I L E - S C O P E ------------------------------------------------*/
#if (MDCP_ETHERNET_USED == 1)
#define MDCP_uw_DECODE_BUFFER_LEN  (uword)1500
#else
#define MDCP_uw_DECODE_BUFFER_LEN  (uword)64
#endif

#define MDCP_LOCKED     (ubyte)1
#define MDCP_UNLOCKED   (ubyte)0

static Bus    MDCP_as_BusObjects[MDCP_uw_MAX_BUSES_USED];
static ubyte  MDCP_aub_InBuffer[MDCP_uw_DECODE_BUFFER_LEN];
static ubyte  MDCP_aub_OutBuffer[MDCP_uw_DECODE_BUFFER_LEN];


static tvoid MDCP_v_FrameHandler           (const Bus* pcs_BusObject);
static tvoid MDCP_v_ErrorHandler           (const Bus* pcs_BusObject);
static tvoid MDCP_v_RouteFrame             (const Bus* pcs_BusObject);
static tvoid MDCP_v_OneShotMeasurement     (const Bus* pcs_BusObject);
static tvoid MDCP_v_ResponseHandler        (const Bus* pcs_BusObject);
static uword MDCP_uw_GetNumberOfFrames     (const uword cuw_BusType,const uword cuw_ToBeSent);






static const FidLink MDCP_acs_FidHandlers[MDCP_ub_MAX_NUMBER_OF_FIDS] =
{
		{MDCP_v_ErrorHandler        , MDCP_ub_ERROR_FID                 , {0xFF,0xFF,0xFF}},
		{MDCP_v_RouteFrame          , MDCP_ub_FWD_FID                   , {0xFF,0xFF,0xFF}},
		{MDCP_v_OneShotMeasurement  , MDCP_ub_ONE_SHOT_MEASUREMENT_FID  , {0xFF,0xFF,0xFF}},
		{MDCP_v_ResponseHandler     , MDCP_ub_RESPONSE_FID              , {0xFF,0xFF,0xFF}},
};

static ubyte MDCP_aub_Locks[MDCP_ub_MAX_NUMBER_OF_FIDS];

static const ubyte MDCP_cub_ReservedOffset = (ubyte)4;

#define MDCP_LockService(ub_Index)   ({if(MDCP_aub_Locks[ub_Index] != MDCP_LOCKED) MDCP_aub_Locks[ub_Index] = MDCP_LOCKED;})
#define MDCP_UnlockService(ub_Index) ({if(MDCP_aub_Locks[ub_Index] != MDCP_UNLOCKED) MDCP_aub_Locks[ub_Index] = MDCP_UNLOCKED;})
/*--------------------------------------- E N D - O F - F I L E - S C O P E ---------------------------------------*/



/*--------------------------------- F U N C T I O N S - I M P L E M E N T A T I O N -------------------------------*/



tvoid MDCP_v_Init(tvoid)
{
	ubyte ub_BusCounter;

	for(ub_BusCounter = (ubyte)0; ub_BusCounter < MDCP_uw_MAX_BUSES_USED; ub_BusCounter++)
	{
		MDCP_as_BusObjects[ub_BusCounter].pub_RxFrame     = null;
		MDCP_as_BusObjects[ub_BusCounter].b_DataReady     = (ubyte)0;
		MDCP_as_BusObjects[ub_BusCounter].uw_BusType      = (uword)0;
		MDCP_as_BusObjects[ub_BusCounter].pfct_Send       = null;
		MDCP_as_BusObjects[ub_BusCounter].uw_BusType      = (ubyte)0xFF;
	}

	UTIL_v_MemSet(&MDCP_aub_Locks[0],MDCP_UNLOCKED,MDCP_ub_MAX_NUMBER_OF_FIDS);
}

/**
 * Cyclic function
 */
tvoid MDCP_v_Run(tvoid)
{
	uword uw_DataSize = (uword)0;
	ubyte ub_BusIndex;
	ubyte *pub_Backup;

	for(ub_BusIndex = (ubyte)0; ub_BusIndex < MDCP_uw_MAX_BUSES_USED; ub_BusIndex++)
	{
		if(MDCP_as_BusObjects[ub_BusIndex].b_DataReady == TRUE)
		{
			MDCP_v_DecodeData(&MDCP_aub_InBuffer[0],MDCP_as_BusObjects[ub_BusIndex].pub_RxFrame,MDCP_cub_ReservedOffset);

			uw_DataSize  = (uword)MDCP_aub_InBuffer[2] << 8;
			uw_DataSize |= MDCP_aub_InBuffer[2];

			if(uw_DataSize <= MDCP_uw_DECODE_BUFFER_LEN)
			{
				MDCP_v_DecodeData(&MDCP_aub_InBuffer[MDCP_cub_ReservedOffset],MDCP_as_BusObjects[ub_BusIndex].pub_RxFrame,uw_DataSize);

				pub_Backup = MDCP_as_BusObjects[ub_BusIndex].pub_RxFrame;
				/* Save location to the frame for latter use */

				MDCP_as_BusObjects[ub_BusIndex].pub_RxFrame = &MDCP_aub_InBuffer[0];

				MDCP_v_FrameHandler(&MDCP_as_BusObjects[ub_BusIndex]);

				MDCP_as_BusObjects[ub_BusIndex].pub_RxFrame = pub_Backup;

			}else
			{
				/* Send an error frame */
			}
		}
	}
}

/**
 * Given the bus object, it requests a region to be allotted
 */
tbool MDCP_b_RequestBusRegion(const Bus* const cpcs_BusObject)
{
	tbool b_RequestState = TRUE;

	if(cpcs_BusObject->uw_BusType < (uword)MDCP_uw_MAX_BUSES_USED)
	{
		MDCP_as_BusObjects[cpcs_BusObject->uw_BusType].pub_RxFrame       = cpcs_BusObject->pub_RxFrame;
		MDCP_as_BusObjects[cpcs_BusObject->uw_BusType].b_DataReady       = cpcs_BusObject->b_DataReady;
		MDCP_as_BusObjects[cpcs_BusObject->uw_BusType].uw_BusType        = cpcs_BusObject->uw_BusType;
		MDCP_as_BusObjects[cpcs_BusObject->uw_BusType].pfct_ErrorHandler = cpcs_BusObject->pfct_ErrorHandler;
		MDCP_as_BusObjects[cpcs_BusObject->uw_BusType].pfct_Route        = cpcs_BusObject->pfct_Route;
		MDCP_as_BusObjects[cpcs_BusObject->uw_BusType].pfct_Send         = cpcs_BusObject->pfct_Send;
		MDCP_as_BusObjects[cpcs_BusObject->uw_BusType].ub_OwnerAddress   = cpcs_BusObject->ub_OwnerAddress;
	}else
	{
		b_RequestState = FALSE;
	}

	return b_RequestState;
}



/**
 * Switch the frame according to its FID
 */
tvoid MDCP_v_FrameHandler(const Bus* pcs_BusObject)
{
	ubyte ub_FID  = *(pcs_BusObject->pub_RxFrame + 0);

	/* Check against the FID validity */
	if(ub_FID < MDCP_ub_MAX_NUMBER_OF_FIDS)
	{
		/* Start handling the proper FID */
		MDCP_LockService(ub_FID);

		MDCP_acs_FidHandlers[ub_FID].pfct_FIDHandler(pcs_BusObject);

		MDCP_UnlockService(ub_FID);

	}else
	{
		/* Send error unknown frame ID */

	}
}

/**
 * Handling an error frame
 */
tvoid MDCP_v_ErrorHandler(const Bus* pcs_BusObject)
{
	pcs_BusObject->pfct_ErrorHandler();
}

/**
 *	In case the application has multiple bus interfaces, it can route the received frame to other application
 */
tvoid MDCP_v_RouteFrame(const Bus* pcs_BusObject)
{
	Frame s_NewFrame;

	s_NewFrame.ub_FID         = *(pcs_BusObject->pub_RxFrame + 0);
	s_NewFrame.ub_SrcDest     = (*(pcs_BusObject->pub_RxFrame + 1));
	s_NewFrame.uw_SizeOfData  = (uword)(*(pcs_BusObject->pub_RxFrame + 2)) << 8;
	s_NewFrame.uw_SizeOfData |= (*(pcs_BusObject->pub_RxFrame + 3));
	s_NewFrame.pub_Data       = (pcs_BusObject->pub_RxFrame + s_NewFrame.uw_SizeOfData);

	if(pcs_BusObject->pfct_Route != null)
	{
		pcs_BusObject->pfct_Route(&s_NewFrame);
	}
}

/**
 * Used to request only a measurement for a component specified in the DATA field
 */
tvoid MDCP_v_OneShotMeasurement(const Bus* pcs_BusObject)
{
	ulong ul_Address   = *(pcs_BusObject->pub_RxFrame + 4);
	ul_Address  <<= (ulong)24;
	ul_Address   |= *(pcs_BusObject->pub_RxFrame + 5);
	ul_Address  <<= (ulong)16;
	ul_Address   |= *(pcs_BusObject->pub_RxFrame + 6);
	ul_Address  <<= (ulong)8;
	ul_Address   |= *(pcs_BusObject->pub_RxFrame + 7);
	ubyte ub_Size = *(pcs_BusObject->pub_RxFrame + 8);
	ubyte ub_Src  = (*(pcs_BusObject->pub_RxFrame + 1) & 0xF0);
	ubyte ub_ByteIndex;
	ubyte *pub_RemoteAddress = (ubyte*)ul_Address;
	uword uw_FrameSize = MDCP_cub_ReservedOffset + ub_Size;
	uword uw_NeededFrames;
	uword uw_BytesPerFrame;
	uword uw_Offset;
	Frame s_Frame;

	for(ub_ByteIndex = (ubyte)0; ub_ByteIndex < ub_Size; ub_ByteIndex++)
	{
		MDCP_aub_OutBuffer[ub_ByteIndex]	= *pub_RemoteAddress++;
	}

	s_Frame.ub_FID      = MDCP_ub_RESPONSE_FID;
	s_Frame.ub_SrcDest  = MDCP_OWNER_ADDRESS;
	s_Frame.ub_SrcDest |= (ub_Src >> 4);

	uw_NeededFrames = MDCP_uw_GetNumberOfFrames(pcs_BusObject->uw_BusType,uw_FrameSize);
	uw_BytesPerFrame = (uw_FrameSize / uw_NeededFrames);

	for(ub_ByteIndex = (ubyte)0; ub_ByteIndex < uw_NeededFrames; ub_ByteIndex++)
	{
		uw_Offset = uw_BytesPerFrame * ub_ByteIndex;

		UTIL_v_MemSet(s_Frame.pub_Data,0x00,(ulong)uw_BytesPerFrame);
		UTIL_v_MemCopy(s_Frame.pub_Data,&MDCP_aub_OutBuffer[uw_Offset],((ulong)uw_BytesPerFrame));

		pcs_BusObject->pfct_Send((ubyte*)&s_Frame,uw_BytesPerFrame);
	}


}

/**
 * Given the bus and the number of bytes to be sent, it calculates the needed frames to be sent
 */
uword MDCP_uw_GetNumberOfFrames(const uword cuw_BusType,const uword cuw_ToBeSent)
{
	uword uw_FramesNeeded = (uword)0;

	switch(cuw_BusType)
	{
#if ((MDCP_FLEXRAY_USED == 1) || (MDCP_KLINE_USED == 1) )

#if (MDCP_FLEXRAY_USED == 1)
	case MDCP_uw_FLEXRAY_BUS :
#endif
#if (MDCP_KLINE_USED == 1)
	case MDCP_uw_KLINE_BUS :
#endif
	{
		if(cuw_ToBeSent > (uword)254)
		{
			if((cuw_ToBeSent % (254)) != (uword)0)
			{
				uw_FramesNeeded = (cuw_ToBeSent / (uword)254) + (uword)1;
			}else
			{
				uw_FramesNeeded = cuw_ToBeSent / (uword)254;
			}

		}else
		{
			uw_FramesNeeded = (uword)1;
		}

		break;
	}
#endif

#if ((MDCP_CAN_USED == 1) || (MDCP_LIN_USED == 1))

#if (MDCP_CAN_USED == 1)
	case MDCP_uw_CAN_BUS :
#endif
#if (MDCP_LIN_USED == 1)
	case MDCP_uw_LIN_BUS :
#endif
	{
		if(cuw_ToBeSent > (uword)8)
		{
			if((cuw_ToBeSent % (uword)8) != (uword)0)
			{
				uw_FramesNeeded = (cuw_ToBeSent / (uword)8) + (uword)1;
			}else
			{
				uw_FramesNeeded = (cuw_ToBeSent / (uword)8);
			}

		}else
		{
			uw_FramesNeeded = (uword)1;
		}

		break;
	}
#endif

#if ((MDCP_USB_USED == 1) || ( MDCP_SPI_USED == 1) || (MDCP_I2C_USED == 1) || (MDCP_CANFD_USED == 1))

#if (MDCP_USB_USED == 1)
	case MDCP_uw_USB_BUS :
#endif
#if ( MDCP_SPI_USED == 1)
	case MDCP_uw_SPI_BUS :
#endif
#if (MDCP_I2C_USED == 1)
	case MDCP_uw_I2C_BUS :
#endif
#if (MDCP_CANFD_USED == 1)
	case MDCP_CANFD_USED :
#endif
	{
		if( cuw_ToBeSent > (uword)64)
		{
			if ((cuw_ToBeSent % (uword)64) != (uword)0)
			{
				uw_FramesNeeded = (cuw_ToBeSent / (uword)64) + (uword)1;
			}else
			{
				uw_FramesNeeded = (cuw_ToBeSent / (uword)64);
			}

		}else
		{
			uw_FramesNeeded = (uword)1;
		}

		break;
	}
	}
#endif

#if(MDCP_ETHERNET_USED == 1)
	case MDCP_uw_ETHERNET_BUS :
	{
		if(cuw_ToBeSent > (uword)1500)
		{
			if((cuw_ToBeSent % (uword)1500) != (uword)0)
			{
				uw_FramesNeeded = (cuw_ToBeSent / (uword)1500) + 1;
			}else
			{
				uw_FramesNeeded = (cuw_ToBeSent / (uword)1500);
			}
		}else
		{
			uw_FramesNeeded = (uword)1;
		}
		break;
#endif
	}

	return (uw_FramesNeeded);

}


tvoid MDCP_v_ResponseHandler(const Bus* pcs_BusObject)
{

}

/*-------------------------- E N D - O F - F U N C T I O N S - I M P L E M E N T A T I O N ------------------------*/



#if (MDCP_DEBUG_MODE == 1)

static tvoid Error(tvoid);
static tvoid Send(const ubyte*,const uword);

static  ubyte array[]= {0x03,0xBA,0x00,0x09,0xFB,0xBC,0xAA,0xF1,0x04};

int main(int argc, const char* argv[])
{
	Bus s_Bus;

	MDCP_v_Init();

	s_Bus.b_DataReady       = 1;
	s_Bus.pfct_ErrorHandler = Error;
	s_Bus.pfct_Send         = Send;
	s_Bus.pfct_Route        = null;
	s_Bus.pub_RxFrame       = array;
	s_Bus.ub_OwnerAddress   = MDCP_OWNER_ADDRESS;
	s_Bus.uw_BusType        = MDCP_uw_CAN_BUS;

	MDCP_b_RequestBusRegion(&s_Bus);

	MDCP_v_Run();


	return EXIT_SUCCESS;
}


tvoid Error(tvoid){}

tvoid Send(const ubyte* data,const uword size)
{
	uword index;

	for(index = 0; index < size; index++)
	{
		printf("%x",data[index]);
	}
}

#endif
